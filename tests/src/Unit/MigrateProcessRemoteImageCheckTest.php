<?php

declare(strict_types = 1);

namespace Drupal\Tests\migrate_process_remote_image_check\Unit\process;

use Drupal\migrate_process_remote_image_check\Plugin\migrate\process\MigrateProcessRemoteImageCheck;
use Drupal\Tests\migrate\Unit\process\MigrateProcessTestCase;
use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use Psr\Log\LoggerInterface;

/**
 * Tests the migrate_process_remote_image_check process plugin.
 *
 * @group migrate_process_remote_image_check
 * @coversDefaultClass \Drupal\migrate_process_remote_image_check\Plugin\migrate\process\MigrateProcessRemoteImageCheck
 */
final class MigrateProcessRemoteImageCheckTest extends MigrateProcessTestCase {

  /**
   * A logger prophecy object.
   *
   * Using ::setTestLogger(), this prophecy will be configured and injected into
   * the container. Using $this->logger->function(args)->shouldHaveBeenCalled()
   * you can assert that the logger was called.
   *
   * @var \Prophecy\Prophecy\ObjectProphecy
   */
  protected $logger;

  /**
   * Mock of http_client service.
   *
   * @var \GuzzleHttp\Client
   */
  protected $mockHttp;

  /**
   * The data fetcher plugin definition.
   */
  private array $pluginDefinition = [
    'id' => 'migrate_process_html',
    'title' => 'Migrate Process HTML',
    'jsredirect' => TRUE,
  ];

  /**
   * Method to toggle the jsredirect state.
   */
  public function toggleJavascriptRedirect() {
    if ($this->pluginDefinition['jsredirect'] === TRUE) {
      $this->pluginDefinition['jsredirect'] = FALSE;
    }
    else {
      $this->pluginDefinition['jsredirect'] = TRUE;
    }
  }

  /**
   * Test MigrateProcessRemoteImageCheck Example1 Pass.
   *
   * Test MigrateProcessRemoteImageCheck returns the original string unaltered
   * for a valid response.
   */
  public function testMigrateProcessRemoteImageCheckExample1Pass() {
    $configuration = [];
    $value_good = 'https://images.lbc.co.uk/images/605914?crop=16_9&width=660&relax=1&format=webp&signature=NMDC95Yh7RrJFuycLD04j52xUeA=';

    $good = file_get_contents(__DIR__ . '/../../fixtures/example1/605914.webp');
    $mock = new MockHandler([
      new Response(200, [], $good),
    ]);
    $handler = HandlerStack::create($mock);
    $this->mockHttp = new Client(['handler' => $handler]);

    $this->logger = $this->prophesize(LoggerInterface::class);

    $value_returned = (new MigrateProcessRemoteImageCheck($configuration, 'migrate_process_remote_image_check', $this->pluginDefinition, $this->mockHttp, $this->logger->reveal()))
      ->transform($value_good, $this->migrateExecutable, $this->row, 'destinationproperty');

    $this->assertEquals($value_good, $value_returned, $message = "actual value is not equal to expected");
  }

  /**
   * Test MigrateProcessRemoteImageCheck Example1 Empty.
   *
   * Test MigrateProcessRemoteImageCheck returns an empty string for a non
   * valid response for an image url string.
   */
  public function testMigrateProcessRemoteImageCheckExample1Empty() {
    $configuration = [];
    $value_bad = 'https://images.lbc.co.uk/images/605914';
    $empty_string = '';

    $bad = file_get_contents(__DIR__ . '/../../fixtures/example1/400_Bad_Request.html');
    $mock = new MockHandler([
      new Response(400, [], $bad),
    ]);
    $handler = HandlerStack::create($mock);
    $this->mockHttp = new Client(['handler' => $handler]);

    $this->logger = $this->prophesize(LoggerInterface::class);

    $value_returned = (new MigrateProcessRemoteImageCheck($configuration, 'migrate_process_remote_image_check', $this->pluginDefinition, $this->mockHttp, $this->logger->reveal()))
      ->transform($value_bad, $this->migrateExecutable, $this->row, 'destinationproperty');

    $this->assertEquals($empty_string, $value_returned, $message = "actual value is not equal to expected");
  }

  /**
   * Test MigrateProcessRemoteImageCheck Example3 Pass.
   *
   * Test MigrateProcessRemoteImageCheck returns the original string unaltered
   * for a valid response.
   */
  public function testMigrateProcessRemoteImageCheckExample3Pass() {
    $configuration = [];
    $value_good = 'https://images.lbc.co.uk/images/605919?width=1200&crop=16_9&signature=35vrQw9Zg953mYC5R8m4MbaUjhs=';

    $good = file_get_contents(__DIR__ . '/../../fixtures/example3/605919.jpeg');
    $mock = new MockHandler([
      new Response(200, [], $good),
    ]);
    $handler = HandlerStack::create($mock);
    $this->mockHttp = new Client(['handler' => $handler]);

    $this->logger = $this->prophesize(LoggerInterface::class);

    $value_returned = (new MigrateProcessRemoteImageCheck($configuration, 'migrate_process_remote_image_check', $this->pluginDefinition, $this->mockHttp, $this->logger->reveal()))
      ->transform($value_good, $this->migrateExecutable, $this->row, 'destinationproperty');

    $this->assertEquals($value_good, $value_returned, $message = "actual value is not equal to expected");
  }

  /**
   * Test MigrateProcessRemoteImageCheck Example 3 Empty.
   *
   * Test MigrateProcessRemoteImageCheck returns an empty string for a non
   * valid response for an image url string.
   */
  public function testMigrateProcessRemoteImageCheckExample3Empty() {
    $configuration = [];
    $value_bad = 'https://images.lbc.co.uk/images/605919?width=1200&amp;crop=16_9&amp;signature=35vrQw9Zg953mYC5R8m4MbaUjhs=';
    $empty_string = '';

    $bad = file_get_contents(__DIR__ . '/../../fixtures/example3/400_Bad_Request.html');
    $mock = new MockHandler([
      new Response(400, [], $bad),
    ]);
    $handler = HandlerStack::create($mock);
    $this->mockHttp = new Client(['handler' => $handler]);

    $this->logger = $this->prophesize(LoggerInterface::class);

    $value_returned = (new MigrateProcessRemoteImageCheck($configuration, 'migrate_process_remote_image_check', $this->pluginDefinition, $this->mockHttp, $this->logger->reveal()))
      ->transform($value_bad, $this->migrateExecutable, $this->row, 'destinationproperty');

    $this->assertEquals($empty_string, $value_returned, $message = "actual value is not equal to expected");
  }

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {

    $this->row = $this->getMockBuilder('Drupal\migrate\Row')
      ->disableOriginalConstructor()
      ->getMock();

    $this->migrateExecutable = $this->getMockBuilder('Drupal\migrate\MigrateExecutable')
      ->disableOriginalConstructor()
      ->getMock();

    $mockHttp = $this
      ->getMockBuilder('GuzzleHttp\Client')
      ->disableOriginalConstructor()
      ->addMethods(['getBody'])
      ->onlyMethods(['get'])
      ->getMock();
    $this->mockHttp = $mockHttp;

    parent::setUp();

  }

}

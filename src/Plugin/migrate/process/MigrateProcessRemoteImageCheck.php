<?php

namespace Drupal\migrate_process_remote_image_check\Plugin\migrate\process;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\migrate\MigrateException;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\TransferException;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'MigrateProcessRemoteImageCheck' migrate process plugin.
 *
 * Example:
 *
 * @code
 * process:
 *   'body/value':
 *     -
 *       plugin: migrate_process_html
 *       enablejs: true // optional defaults to true
 *       source: link
 *     -
 *       plugin: dom
 *       method: import
 *     -
 *       plugin: dom_select
 *       selector: //meta[@property="og:image"]/@content
 *     -
 *       plugin: skip_on_empty
 *       method: row
 *       message: 'Field image is missing'
 *     -
 *       plugin: extract
 *       index:
 *         - 0
 *     -
 *       plugin: skip_on_condition
 *       method: row
 *       condition:
 *         plugin: not:matches
 *       regex: /^(https?:\/\/)[\w\d]/i
 *       message: 'We only want a string if it starts with http(s)://[\d\w]'
 *     -
 *       plugin: migrate_process_remote_image_check
 *       remove_query_string: true # Default false
 *     -
 *       plugin: file_remote_url
 *
 * @endcode
 *
 * @see \Drupal\migrate\Plugin\MigrateProcessInterface
 *
 * @MigrateProcessPlugin(
 *  id = "migrate_process_remote_image_check"
 * )
 */
class MigrateProcessRemoteImageCheck extends ProcessPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * The Logger Class object.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Returns the HTTP client.
   *
   * @return \GuzzleHttp\ClientInterface
   *   The Guzzle client.
   */
  public function getHttpClient() {
    if (!isset($this->httpClient)) {
      $this->httpClient = \Drupal::httpClient();
    }
    return $this->httpClient;
  }

  /**
   * Checks if url starts with http.
   *
   * Checks if url matches FILTER_VALIDATE_URL. Also Checks if url starts with
   * http(s):// and is followed by a letter or number.
   *
   * @param string $url
   *   The url string to check.
   *
   * @return bool
   *   TRUE if URL string is valid.
   */
  public function checkUrlIsValid($url) {
    $regex_url = '/^(https?:\/\/)[\w\d]/i';

    if (strpos($url, 'http') === 0
    && filter_var($url, FILTER_VALIDATE_URL) !== FALSE
    && preg_match($regex_url, $url) === 1) {
      return TRUE;
    }

    else {
      return FALSE;
    }

  }

  /**
   * Constructs a database object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The Guzzle HTTP client.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ClientInterface $http_client, LoggerInterface $logger) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->httpClient = $http_client;
    $this->logger = $logger;
    $configuration += [
      'guzzle_options' => [],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {

    $configuration = $this->configuration;
    // $regex_url = '/^(https?:\/\/)[\w\d]/i';
    $checkQuery = FALSE;
    // Check for remove_query_string config setting and override if set.
    if (isset($configuration["remove_query_string"])) {
      $checkQuery = TRUE;
    }

    // Step 1 - default.
    if ($checkQuery !== TRUE) {
      // Check if url is absolute (not relative) and is a url.
      if ($this->checkUrlIsValid($value)) {
        try {
          $response = $this->getHttpClient()->request('GET', $value, $configuration);
          $response->getBody();
        }
        catch (TransferException $e) {
          // Logs a notice to channel if we get http error response.
          $this->logger->notice('Failed to get (1) URL @url "@error". @code', [
            '@url' => $value,
            '@error' => $e->getMessage(),
            '@code' => $e->getCode(),
          ]);
          // This implies that the url is not valid so lets run step 2.
          $checkQuery = TRUE;
        }
      }
      else {
        // If string does not start with http return empty string.
        return '';
      }
    }

    // Step 2 - Strip query String.
    if ($checkQuery) {
      // Need to check again as step one can be bypassed if remove_query_string
      // is enabled (TRUE)
      if ($this->checkUrlIsValid($value)) {

        $regex_query_string = '/^(.*)\?/';
        $haystack = $value;
        $needle = $regex_query_string;
        // Lets try remove the query string and see if we get an OK response.
        $value_without_query_string = $this->matchFirstRegExpr($needle, $haystack);
        try {
          $response = $this->getHttpClient()->request('GET', $value_without_query_string, $configuration);
          $response->getBody();
        }
        catch (\Throwable $e) {
          $this->logger->notice('Failed to get (2) URL @url "@error". @code', [
            '@url' => $value,
            '@error' => $e->getMessage(),
            '@code' => $e->getCode(),
          ]);
          // Lets return an empty string as all checks have failed.
          return '';
        }

        // If we have not returned an empty string the request was successful.
        return $value_without_query_string;
      }

    }
    // Step 3. - If all checks pass.
    else {
      // All other checks pass, so let's return original value.
      return $value;
    }
  }

  /**
   * Returns first match for regular expression.
   *
   * @param string $needle
   *   Reg expression.
   * @param string $haystack
   *   Source that we use to parse reg expression.
   *
   * @return string
   *   Subpattern match or original haystack.
   */
  protected function matchFirstRegExpr($needle, $haystack) {
    if (is_string($haystack)) {
      preg_match($needle, $haystack, $match);
      // https://www.php.net/manual/en/function.preg-match.php
      // If matches is provided, then it is filled with the results of search.
      // $matches[0] will contain the text that matched the full pattern,
      // $matches[1] will have the text that matched the first captured
      // parenthesized subpattern, and so on.
      if (isset($match) && isset($match[0])) {
        return $match[1];
      }
      else {
        return $haystack;
      }
    }
    else {
      throw new MigrateException('When using the regex matches condition, the source must be a string.');
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('http_client'),
      $container->get('logger.channel.migrate_process_html')
    );
  }

}
